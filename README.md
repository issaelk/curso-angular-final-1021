# Spotify Lite

Este proyecto es la propuesta de proyecto final para el Curso de Angular de Escalab 2021.
**Autor:** Issael Falcón

# Descripción

Utilizando el API pública de [Spotify](https://developer.spotify.com/documentation/web-api/quick-start/) , esta app muestra recomendaciones basadas en países así como "New Releases". Se integra un buscador para artistas, canciones, playlist y albums. 

Se realizó una visualización de igual manera para esos conceptos en donde es posible tener una cadena de pantallas, por ejemplo, de un artista, se pueden mostrar sus albums y se liga a ellos.

Para comenzar en la app se pide un "login" este login es fake solo se requiere cumplir con las condiciones de colocar un email con formato válido así como una contraseña con el REGEX solicitado. Una vez colocados esos datos se genera un token que permite la comunicación con Spotify.

Se tomó la decisión de utilizar ionic ya que la intención era tener las herramientas para realizar una app, de igual forma se usó como base Angular, es decir el proyecto quedó basado en [ionic + angular](https://ionicframework.com/angular) .

## Requisitos

### Arquitectura (directorio shared, pages, components y demás).
Se utilizó la estructura solicitada.

### Uso de módulos.
Para la creación de las diferentes pantallas se hizo uso de módulos y componentes que permite la comunicación con ellos mediante el routing. 

### Manejo de rutas.
Se hizo una estructura de rutas:

- welcome (public)
- p404 (public)
- tabs
	- home
	- categories
	- recommend
	- search
- include
	- artist
	- album
	- category
	- playlist

### Lazy load.
Se hizo uso de la carga perezosa en todas las rutas.

### Uso de servicios y consumo de apis.
Al hacer uso del API de Spotify fue necesario consumirlo. Para realizarlo se implementaron dos formas:

 - Plugin de Spotify (https://github.com/jmperez/spotify-web-api-js)
 - Cliente HTTP de Angular
	 - Se utilizó en la pantalla y funcionalidad de búsqueda mediante **Observables**

Todo esto con el uso de un servicio llamado SpotifyService.

### Uso de pipes y directivas.
Se utilizaron pipes que están incluidas en angular como **uppercase** (ejemplo en detalle de album). 

De igual manera se generaron dos directivas personalizadas:

 - backButton: Para hacer uso del boton de fecha "para atrás" que maneja una lista de historial en la que permite moverse entre pantallas.
 - goTo: Esta directiva permite recibir dos parámetros, uno de tipo (playlist, album, track, artist) y un id. Hace la redirección a la ruta correspondiente.

### Comunicación entre componentes.
[TODO]  Faltó encontrar una "buena excusa" para usarlo en este proyecto...

### Uso de formularios reactivos.
En la pantalla de Welcome se presenta un pequeño formulario con inputs tipo text y password. Esta tiene las reglas de regex y de que sea ingresado. Se utilizó de igual manera los mensajes de error personalizados.

### Implementación de página not found 404.
Al acceder a una ruta no existente, mostrará una pantalla de no existencia.

### Uso de guards, aplicar por lo menos 2.
Se utilizaron dos guards:

 - canActivate: Verifica que exista un token para acceder a la api de spotify. En caso de no existir es redireccionado a la pantalla de welcome, la cual luego de ingresar el formulario genera el token y permite el acceso.
 - resolve: Fue dificil encontrar una función para este, pero con ello se carga la pantalla de categories, esta busca en el API las categorias y la manda a la ruta, es decir, la pantalla de categories primero carga las categorias y al tener esa información carga la ruta.

### Uso correcto de hooks (eventos del ciclo de vida), aplicar por lo menos 2..
Se utilizaron el onInit en varias instancias así como el onDestroy, este particularmente en la pantalla de búsqueda para cerrar el subscribe utilizado.

### Uso de patrones utilizados en el curso, aplicar por lo menos 2.
Se pueden ubicar dentro de todo el proyecto diferentes patrones utilizados

## Instalación

 1. Descargar el proyecto.
 2. Iniciar con `npm install` para cargar las librerías y herramientas utilizadas en el proyecto.
 3. Lanzarlo utilizando el comando `ionic serve`

## Demo online

Para acceder al demo se puede realizar en esta ruta:
https://cuijjisw.com/spotify-lite/

## Comentarios adicionales
El api de spotify es pública pero requiere un token con una app con ellos. Para evitar el tener que generar esa app, se realizó un código en un servidor que permite obtener un token con un uso limitado por 6 minutos.

Un trabajo adicional se podría hacer para manejar la expiración de dicho token, por el momento, si deja de estar válido, el usuario será retornado al home para recomenzar el proceso.