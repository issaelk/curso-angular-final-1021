import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import SpotifyWebApi from 'spotify-web-api-js';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class SpotifyService {



  private urlApiSpotify: string = 'https://api.spotify.com/v1/';
  private urlTokenGenerator: string = 'https://bitso.cuijjisw.com/spotify.php';
  private token: string;
  private spotifyClient = new SpotifyWebApi();


  constructor(private _httpClient: HttpClient, private _router: Router) {
    
    //this.spotifyClient.setPromiseImplementation(Q);
    this.spotifyClient.setAccessToken(this.getToken());
  }

  private getToken(){
  
    this.token = localStorage.getItem('spotify_token');

    if(this.token == undefined){
      this._router.navigate(['/home']).then(() => false);
    }
    
    return this.token;
  }

  async requestAccessToken(){
    /*this._httpClient.get<any[]>(this.urlTokenGenerator).subscribe(
      (response:any) => {
        this.token = response.access_token;
        localStorage.setItem('spotify_token', this.token);
        this.spotifyClient.setAccessToken(this.getToken());
        console.log(this.token);
        return true;
      }
    );*/

    var response = await this._httpClient.get<any[]>(this.urlTokenGenerator).toPromise();
    this.token = response["access_token"];
    localStorage.setItem('spotify_token', this.token);
    this.spotifyClient.setAccessToken(this.getToken());
    return this.token;
    
  }

  public hasTokenActive(){
    
    let token = localStorage.getItem('spotify_token');
    return !(token == undefined);

  }

  public getCategories(){
    var params = {
      country: 'MX',
      locale: 'es_MX',
      limit: 50
    };
    var info = this.spotifyClient.getCategories(params);
    info.catch((err) => {
      this.requestAccessToken();
    });
    return info;
  }

  public getCategoryPlaylist(id){
    var params = {
      country: 'MX',
      limit: 50
    };
    var info = this.spotifyClient.getCategoryPlaylists(id,params);
    info.catch((err) => {
      this.requestAccessToken();
    });
    return info;
  }

  public getCategory(id){
    var info = this.spotifyClient.getCategory(id);
    info.catch((err) => {
      this.requestAccessToken();
    });
    return info;
  }

  public getArtistsInfo(id){
    var info = this.spotifyClient.getArtist(id);
    info.catch((err) => {
      this.requestAccessToken();
    });
    return info;
  }

  public getInfoPlaylist(id){
    var info = this.spotifyClient.getPlaylist(id);
    info.catch((err) => {
      this.requestAccessToken();
    });
    return info;
  }

  public getArtistTopTracks(id){
    var info = this.spotifyClient.getArtistTopTracks(id,'MX');
    info.catch((err) => {
      this.requestAccessToken();
    });
    return info;
  }

  public getAlbumsArtist(id){
    var info = this.spotifyClient.getArtistAlbums(id);
    info.catch((err) => {
      this.requestAccessToken();
    });
    return info;
  }

  public getInfoAlbum(id){
    var info = this.spotifyClient.getAlbum(id);
    info.catch((err) => {
      this.requestAccessToken();
    });
    return info;
  }

  public getNewReleases(){
    var params = {
      limit: 5
    }
    var info = this.spotifyClient.getNewReleases(params);
    info.catch((err) => {
      this.requestAccessToken();
    });
    return info; 
  }

  public getRecommendedPlaylist(country){
    var params = {
      country: country,
      limit: 5
    }
    var info = this.spotifyClient.getFeaturedPlaylists(params);
    info.catch((err) => {
      this.requestAccessToken();
    });
    return info; 
  }


  public doSearch(info): Observable<Array<any>>{
    let params = new HttpParams();
    let headers = new HttpHeaders();
    headers = headers.append('Authorization',`Bearer ${this.getToken()}`)
    params = params.append('q', info.text);
    params = params.append('type', info.types.join(','));
    params = params.append('limit', 4);
    return this._httpClient.get<any[]>(this.urlApiSpotify+"search", { params: params, headers: headers });
  }


}
