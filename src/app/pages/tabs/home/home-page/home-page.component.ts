import { Component, OnInit } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { SpotifyService } from 'src/app/service/spotify.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit {

  public info: Array<any> = [];
  
  public albums = [];

  constructor(private _spotifyService: SpotifyService) { }

  ngOnInit() {
    this.getNewReleases();
    this.populateRecommendedPlaylist();
  }

  public recommendedPlaylist = [
    {
      country: 'MX',
      title: 'México',
      playlist: []
    },{
      country: 'US',
      title: 'Estados Unidos',
      playlist: []
    },{
      country: 'CO',
      title: 'Colombia',
      playlist: []
    },{
      country: 'CL',
      title: 'Chile',
      playlist: []
    },
  ];

  opts = {
    slidesPerView: 3.5,
    slidesOffsetBefore: 6,
    spaceBetween: 20,
    freeMode: false
  };

  private getNewReleases(): void{
    this._spotifyService.getNewReleases().then((res) => {
      this.albums = res.albums.items;
    });
  }

  private populateRecommendedPlaylist(){
    for (let index = 0; index < this.recommendedPlaylist.length; index++) {
      const element = this.recommendedPlaylist[index];
      this._spotifyService.getRecommendedPlaylist(this.recommendedPlaylist[index].country).then((res) => {
        element.playlist = res.playlists.items;
        element.title = res.message;
      });
    }
  }

}
