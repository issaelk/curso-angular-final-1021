import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { IonSearchbar } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { SpotifyService } from 'src/app/service/spotify.service';


@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss'],
})
export class SearchPageComponent implements OnInit, OnDestroy {


  @ViewChild('search') searchbar1: IonSearchbar;

  public search = {
    text: '',
    types: []
  };

  private suscription: Subscription = new Subscription();

  public searchsType: Array<any> = [{
    title: 'Artists',
    code: 'artist',
    selected: true,
    icon:'person'
  },{
    title: 'Albums',
    code: 'album',
    selected: true,
    icon: 'disc'
  },{
    title: 'Playlists',
    code: 'playlist',
    selected: true,
    icon: 'apps'
  },{
    title: 'Songs',
    code: 'track',
    selected: true,
    icon: 'musical-notes'
  }];

  public searchResults = {
    albums: [],
    artists: [],
    tracks: [],
    playlist: []
  };

 
  

  constructor(public _toastController: ToastController, private _spotifyService: SpotifyService) { }

  ngOnInit() {}

  ngOnDestroy(): void{
    this.suscription.unsubscribe();
  }

  public selectType(t){
    for (let index = 0; index < this.searchsType.length; index++) {
      const element = this.searchsType[index];
      if(element.code == t){
        element.selected = !element.selected;
      }
    }
    this.doSearch();
  }

  public changeInput(){
    
    this.searchbar1.getInputElement().then( data => {
      this.search.text = data.value;
      this.doSearch();
    });


  }

  public  doSearch(){


    if(this.search.text.length <= 3){
      return false;
    }

    //this.sendError("You need to enter at least 3 characters.");

    var itemSelected = [];

    for (let index = 0; index < this.searchsType.length; index++) {
      const element = this.searchsType[index];
      if(element.selected){
        itemSelected.push(element.code);
      }
    }
 
    

    if(itemSelected.length == 0){
      this.sendError("You must choose at least one category");
      return false;
    }

    this.search.types = itemSelected;


    this.suscription = this._spotifyService.doSearch(this.search).subscribe(
      (response: any) => {
        
          

        if(response.albums){
          this.searchResults.albums = response.albums.items;
        }else{
          this.searchResults.albums = [];
        }

        if(response.artists){
          this.searchResults.artists = response.artists.items;
        }else{
          this.searchResults.artists = [];
        }
        
        if(response.playlists){
          this.searchResults.playlist = response.playlists.items;
        }else{
          this.searchResults.playlist = [];
        }
        
        if(response.tracks){
          this.searchResults.tracks = response.tracks.items;
        }else{
          this.searchResults.tracks = [];
        }

      },
      (error) => {
        console.log(error)
      }
    );

  }

  public async sendError(text){
    const toast = await this._toastController.create({
      message: text,
      duration: 2000
    });
    toast.present();
  }

}
