import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriesRoutingModule } from './categories-routing.module';
import { IonicModule } from '@ionic/angular';
import { CategoriesPageComponent } from './categories-page/categories-page.component';


@NgModule({
  declarations: [CategoriesPageComponent],
  imports: [
    IonicModule,
    CommonModule,
    CategoriesRoutingModule
  ]
})
export class CategoriesModule { }
