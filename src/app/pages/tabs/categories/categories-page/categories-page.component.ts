import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SpotifyService } from 'src/app/service/spotify.service';

@Component({
  selector: 'app-categories-page',
  templateUrl: './categories-page.component.html',
  styleUrls: ['./categories-page.component.scss'],
})
export class CategoriesPageComponent implements OnInit {

  constructor(private _spotifyService: SpotifyService, private router: Router, private _activatedRoute: ActivatedRoute) { }

  public categories;

  ngOnInit() {
    // Cambiamos esta línea para usar el guard de resolver.
    this.categories = this._activatedRoute.snapshot.data.categories.categories.items;
    //this.getCategories();
  }


  public accederCategoria(id): void {
    this.router.navigate([`/include/category/${id}`]);
  }

  private getCategories(): void{
    this._spotifyService.getCategories().then((res) => {
      this.categories = res.categories.items;
    });
  }
  
}
