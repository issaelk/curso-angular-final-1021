import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecommendRoutingModule } from './recommend-routing.module';
import { IonicModule } from '@ionic/angular';
import { RecommendPageComponent } from './recommend-page/recommend-page.component';


@NgModule({
  declarations: [RecommendPageComponent],
  imports: [
    IonicModule,
    CommonModule,
    RecommendRoutingModule
  ]
})
export class RecommendModule { }
