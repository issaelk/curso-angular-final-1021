import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaylistRoutingModule } from './playlist-routing.module';
import { PlaylistPageComponent } from './playlist-page/playlist-page.component';
import { IonicModule } from '@ionic/angular';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';


@NgModule({
  declarations: [PlaylistPageComponent],
  imports: [
    IonicModule,
    CommonModule,
    PlaylistRoutingModule,
    DirectivesModule
  ]
})
export class PlaylistModule { }
