import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from 'src/app/service/spotify.service';

@Component({
  selector: 'app-playlist-page',
  templateUrl: './playlist-page.component.html',
  styleUrls: ['./playlist-page.component.scss'],
})
export class PlaylistPageComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, private _spotifyService: SpotifyService) { }

  public playlist;
  public data = false;

  ngOnInit() {
    const idAlbum = this.activatedRoute.snapshot.paramMap.get('id');
    this.getPlaylistInfo(idAlbum); 
  }

  private async getPlaylistInfo(idAlbum){
    await this._spotifyService.getInfoPlaylist(idAlbum).then((res) => {
      this.playlist = res;
      this.data = true;
      console.log(this.playlist);
    });

  }


}
