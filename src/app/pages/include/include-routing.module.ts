import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{
  path:'playlist/:id',
  loadChildren: () => import('./playlist/playlist.module').then(m => m.PlaylistModule)
},{
  path:'album/:id',
  loadChildren: () => import('./album/album.module').then(m => m.AlbumModule)
},{
  path:'artist/:id',
  loadChildren: () => import('./artist/artist.module').then(m => m.ArtistModule)
},{
  path:'category/:id',
  loadChildren: () => import('./category/category.module').then(m => m.CategoryModule)
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IncludeRoutingModule { }
