import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SpotifyService } from 'src/app/service/spotify.service';

@Component({
  selector: 'app-category-page',
  templateUrl: './category-page.component.html',
  styleUrls: ['./category-page.component.scss'],
})
export class CategoryPageComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, private _spotifyService: SpotifyService, private router: Router) { }

  public playlist;
  public category;
  public data = false;

  ngOnInit() {
    const idCategory = this.activatedRoute.snapshot.paramMap.get('id');
    this.getCategory(idCategory);
    this.getCategoryPlaylist(idCategory);
  }

  private async getCategory(idArtist){
    await this._spotifyService.getCategory(idArtist).then((res) => {
      this.category = res;
    });
  }

  private async getCategoryPlaylist(idArtist){
    await this._spotifyService.getCategoryPlaylist(idArtist).then((res) => {
      this.playlist = res.playlists.items;
      this.data = true;
    });
  }

  public accederPlaylist(id){
    this.router.navigate([`/include/playlist/${id}`]);
  }


}
