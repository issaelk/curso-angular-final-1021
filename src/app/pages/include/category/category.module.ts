import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoryRoutingModule } from './category-routing.module';
import { IonicModule } from '@ionic/angular';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { CategoryPageComponent } from './category-page/category-page.component';


@NgModule({
  declarations: [CategoryPageComponent],
  imports: [
    CommonModule,
    CategoryRoutingModule,
    IonicModule,
    DirectivesModule
  ]
})
export class CategoryModule { }
