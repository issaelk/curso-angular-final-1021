import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavigationService } from 'src/app/service/navigation.service';
import { SpotifyService } from 'src/app/service/spotify.service';
import { Location } from '@angular/common'

@Component({
  selector: 'app-album-page',
  templateUrl: './album-page.component.html',
  styleUrls: ['./album-page.component.scss'],
})
export class AlbumPageComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, private _spotifyService: SpotifyService, private location: Location) { }

  public album: SpotifyApi.SingleAlbumResponse = ({} as any) as  SpotifyApi.SingleAlbumResponse;
  public data = false;

  ngOnInit() {
    const idAlbum = this.activatedRoute.snapshot.paramMap.get('id');
    this.getAlbumInfo(idAlbum);
    
  }

  private async getAlbumInfo(idAlbum){
  console.log(this.album);
    await this._spotifyService.getInfoAlbum(idAlbum).then((res) => {
      this.album = res;
      this.data = true;
      console.log(this.album);
    });

  }



}
