import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlbumRoutingModule } from './album-routing.module';
import { IonicModule } from '@ionic/angular';
import { AlbumPageComponent } from './album-page/album-page.component';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';


@NgModule({
  declarations: [AlbumPageComponent],
  imports: [
    IonicModule,
    CommonModule,
    AlbumRoutingModule,
    DirectivesModule
  ]
})
export class AlbumModule { }
