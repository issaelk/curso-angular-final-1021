import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from 'src/app/service/spotify.service';


@Component({
  selector: 'app-artist-page',
  templateUrl: './artist-page.component.html',
  styleUrls: ['./artist-page.component.scss'],
})
export class ArtistPageComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, private _spotifyService: SpotifyService) { }

  public artist;
  public data = false;
  public albums;
  public topTracks;

  ngOnInit() {
    const idArtist = this.activatedRoute.snapshot.paramMap.get('id');
    this.getArtistsInfo(idArtist);
    this.getAlbumsArtist(idArtist);
    this.getArtistTopTracks(idArtist);
  }

  private async getArtistsInfo(idArtist){
    await this._spotifyService.getArtistsInfo(idArtist).then((res) => {
      this.artist = res;
    });
  }
  private async getArtistTopTracks(idArtist){
    await this._spotifyService.getArtistTopTracks(idArtist).then((res) => {
      this.topTracks = res.tracks;
    });
  }

  private async getAlbumsArtist(idArtist){
    await this._spotifyService.getAlbumsArtist(idArtist).then((res) => {
      this.albums = res.items;
      this.data = true;
    });
  }


}
