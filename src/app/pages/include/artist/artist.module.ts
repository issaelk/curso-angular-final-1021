import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArtistRoutingModule } from './artist-routing.module';
import { IonicModule } from '@ionic/angular';
import { ArtistPageComponent } from './artist-page/artist-page.component';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';

import { ComponentsModule } from 'src/app/shared/components/components.module';


@NgModule({
  declarations: [ArtistPageComponent],
  imports: [
    IonicModule,
    CommonModule,
    ArtistRoutingModule,
    DirectivesModule,
    ComponentsModule
  ]
})
export class ArtistModule { }
