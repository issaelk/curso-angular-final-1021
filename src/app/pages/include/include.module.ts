import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IncludeRoutingModule } from './include-routing.module';
import { IonicModule } from '@ionic/angular';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';


@NgModule({
  declarations: [],
  imports: [
    IonicModule,
    CommonModule,
    IncludeRoutingModule,
    DirectivesModule
  ]
})
export class IncludeModule { }
