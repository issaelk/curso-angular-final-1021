import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SpotifyService } from 'src/app/service/spotify.service';

@Component({
  selector: 'app-welcome-page',
  templateUrl: './welcome-page.component.html',
  styleUrls: ['./welcome-page.component.scss'],
})
export class WelcomePageComponent implements OnInit {

  loginForm: FormGroup;
  isSubmitted = false;
  isSending = false;

  constructor(public formBuilder: FormBuilder, private _spotifyService: SpotifyService, private _router: Router) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      password: ['', [
        Validators.required, 
        Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}$")
     ]],
    })
  }

  get errorControl() {
    return this.loginForm.controls;
  }


  async submitForm() {
    this.isSending = true;
    this.isSubmitted = true;
    console.log(this.loginForm.value)
    if (!this.loginForm.valid) {
      this.isSending = false;
      return false;
    } else {
      let token = await this._spotifyService.requestAccessToken();
      this.isSending = false;

      if(this._spotifyService.hasTokenActive()){
        //Redirect
        return this._router.navigate(['/home']).then(() => false);
      }else{
        alert("Hubo un error, inténtalo mas tarde.");
      }
      
      return true;
      
    }
  }

}
