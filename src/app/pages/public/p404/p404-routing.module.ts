import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { P404PageComponent } from './p404-page/p404-page.component';

const routes: Routes = [{
  path: '',
  component: P404PageComponent,
  children: [
    {path:'', component: P404PageComponent}
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class P404RoutingModule { }
