import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { P404RoutingModule } from './p404-routing.module';
import { IonicModule } from '@ionic/angular';
import { P404PageComponent } from './p404-page/p404-page.component';


@NgModule({
  declarations: [P404PageComponent],
  imports: [
    CommonModule,
    P404RoutingModule,
    IonicModule
  ]
})
export class P404Module { }
