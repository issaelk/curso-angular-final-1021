import { Directive, HostListener, Input } from '@angular/core';
import { Router } from '@angular/router';

@Directive({
  selector: '[appGoTo]'
})
export class GoToDirective {

  constructor(private _router: Router) {}
  
  @Input() typeRedirect = '';
  @Input() idRedirect = '';


  @HostListener('click')
  onClick(): void {

    
    switch(this.typeRedirect){
      case 'playlist': this._router.navigate([`/include/playlist/${this.idRedirect}`]); break;
      case 'album': this._router.navigate([`/include/album/${this.idRedirect}`]); break;
      case 'track': this._router.navigate([`/include/album/${this.idRedirect}`]); break;
      case 'artist': this._router.navigate([`/include/artist/${this.idRedirect}`]); break;
    }

  }

}
