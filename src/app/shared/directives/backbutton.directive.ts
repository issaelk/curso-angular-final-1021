import { Directive, ElementRef, HostListener } from '@angular/core';
import { NavigationService } from 'src/app/service/navigation.service';

@Directive({
  selector: '[appBackButton]'
})
export class BackbuttonDirective {

  constructor(private navigation: NavigationService) {
  }

  @HostListener('click')
  onClick(): void {
    console.log("entré :DD");
    this.navigation.back()
  }

}
