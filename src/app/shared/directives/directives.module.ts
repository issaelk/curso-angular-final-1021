import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackbuttonDirective } from './backbutton.directive';
import { GoToDirective } from './go-to.directive';




@NgModule({
  declarations: [BackbuttonDirective, GoToDirective],
  exports:[BackbuttonDirective, GoToDirective],
  imports: [
    CommonModule
  ]
})
export class DirectivesModule { }
