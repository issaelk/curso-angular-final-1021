import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-artwork-display',
  templateUrl: './artwork-display.component.html',
  styleUrls: ['./artwork-display.component.scss'],
})
export class ArtworkDisplayComponent implements OnInit {

  constructor(private router: Router) {}

  opts = {
    slidesPerView: 3.5,
    slidesOffsetBefore: 6,
    spaceBetween: 20,
    freeMode: false
  };

  @Input() item: any;
  @Input() title: string;
  @Input() type: string;

  ngOnInit() {}

  public getItemList(){
    
    switch(this.type){
      case 'playlist': return this.item.playlist;
      case 'album': return this.item;
    }
  }

  public accessNode(id){
    
    switch(this.type){
      case 'playlist': this.router.navigate([`/include/playlist/${id.id}`]); break;
      case 'album': this.router.navigate([`/include/album/${id.id}`]); break;
    }
    

  }

}
