import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArtworkDisplayComponent } from './artwork-display/artwork-display.component';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [ArtworkDisplayComponent],
  exports:[ArtworkDisplayComponent],
  imports: [
    CommonModule,
    IonicModule
  ]
})
export class ComponentsModule { }
