import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { CategoryResolver } from './guards/categoryResolver.guard';
import { TokenActiveGuard } from './guards/token-active.guard';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'include',
        canActivate: [TokenActiveGuard],
        loadChildren: () => import('./pages/include/include.module').then(m => m.IncludeModule)
      },
      {
        path: 'home',
        canActivate: [TokenActiveGuard],
        loadChildren: () => import('./pages/tabs/home/home.module').then(m => m.HomeModule)
      },
      {
        path: 'search',
        canActivate: [TokenActiveGuard],
        loadChildren: () => import('./pages/tabs/search/search.module').then(m => m.SearchModule)
      },
      {
        path: 'categories',
        canActivate: [TokenActiveGuard],
        resolve:{
          categories: CategoryResolver
        },
        loadChildren: () => import('./pages/tabs/categories/categories.module').then(m => m.CategoriesModule)
      },
      {
        path: 'recommend',
        canActivate: [TokenActiveGuard],
        loadChildren: () => import('./pages/tabs/recommend/recommend.module').then(m => m.RecommendModule)
      },
      {
        path: 'welcome',
        loadChildren: () => import('./pages/public/welcome/welcome.module').then(m => m.WelcomeModule)
      },
      {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
      },
      { path: '**', pathMatch: 'full', 
        loadChildren: () => import('./pages/public/p404/p404.module').then(m => m.P404Module)
      }
    ]
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
