import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/internal/Observable";
import { SpotifyService } from "../service/spotify.service";

@Injectable({ providedIn: 'root' })
export class CategoryResolver implements Resolve<any> {
  constructor(private _spotfyService: SpotifyService) {}

  async resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<Observable<any> | Promise<any> | any> {
    var categories = await this._spotfyService.getCategories();
    return categories;
  }
}